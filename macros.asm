
.macro copyChars(){
    ldy #ceil((musicload-karload)/256)//#15//ceil(sid.size/256)
    ldx #0
loop0:    
    lda karload,x
    sta KAR_TARGET,x
    inx
    bne loop0
    inc loop0+2
    inc loop0+5
    dey
    bne loop0 
}
.macro copysid() {
    //copy sid
    ldy #ceil(sid.size/256)//#15//ceil(sid.size/256)
    ldx #0
loop0:    
    lda musicload,x
    sta sid.location,x
    inx
    bne loop0
    inc loop0+2
    inc loop0+5
    dey
    bne loop0 

}
.macro copyCharCol() {
    ldy #4
    ldx #0
loop1:    
    lda picload+8000,x
    sta charAt,x
    inx
    bne loop1
    inc loop1+2
    inc loop1+5
    dey
    bne loop1 
}
.macro  copybitmap(){
    ldy #32
    ldx #0
loop2:    
    lda picload+31*256,x
    sta bitmapAt+31*256,x
    inx
    bne loop2
    dec loop2+2
    dec loop2+5
    dey
    bne loop2 
}
.macro copyD800() {
    ldy #4
    ldx #0
loop3:    
    lda picload+9000,x
    sta $d800,x
    inx
    bne loop3
    inc loop3+2
    inc loop3+5
    dey
    bne loop3 

}

.macro setBitmap(){
        //setBitmap at $4000/chars$6000
    lda # ((bitmapAt ^ $ffff) >> 14)
    sta $dd00
    lda # (((charAt & $3fff) / $0400) << 4) + (((bitmapAt & $3fff) / $0800) << 1)
    sta $d018

    lda #$3b    //standard 1b + $20
    sta $d011
    lda #$C8//#$d8
    sta $d016

}
.macro clearSpriteArea(){
    ldy #2
    lda #0
    ldx #0
lp:  sta SPRITE_START,x
    inx
    bne lp
    inc lp+2
    dey
    bne lp
}

.macro scrollSpriteData(){
    lda bufferCount
    bne noBufferFill
    ldx #0
    stx sm + 2
    ldy curchar
    lda curchar +1
    clc
    adc #>scrollText
    sta sm0 + 2
sm0:    
    lda scrollText,y
    asl 
    rol sm+2
    asl 
    rol sm+2
    asl 
    rol sm+2
    sta sm +1
    lda sm+2
    clc
    adc #>KAR_TARGET
    sta sm+2
sm:
    lda $ffff,x
    sta BUFFER,x
    inx
    cpx #8
    bne sm
    inc curchar
    bne noBufferFill
    lda curchar + 1
    eor #$01
    sta curchar + 1
noBufferFill:
    .for(var y=4; y<12; y++) {
        clc
        rol BUFFER + y-4
        .for(var s=7; s>=0 ;s--) {
            rol SPRITE_START + s*64 + y*3 + 2
            rol SPRITE_START + s*64 + y*3 + 1
            rol SPRITE_START + s*64 + y*3
        }
    }
    lda bufferCount
    clc
    adc #1
    and #$07
    sta bufferCount

}

.macro setupSprites(){
    //setup sprites
    ldy #$4
.var c=0    
    .for(var i=0; i<8;i++)
    {
        .if(i==0) {
@spriteZeroPos:            
            lda #238
        }else {
            lda #i*48+246//lda #i*48-10
        }   

        
        sta $d000+i*2
        sty $d001+i*2
        lda #SPRITE_POINTER_START+i
        sta $63f8+i
        lda #SPRITE_COLOR
        sta $d027+i
    }
    lda #%11000001
    sta $d010
    .for(var i=0;i<8; i++){
        lda #floor(random()*256)
        sta BUFFER + i
    }
}
.macro setupIRQ() {
    ldx #$ff
    stx $d01d   //expand X

    lda #$00
    sta $d017   //expand Y
    sta $d01b //sprite priority
    sta $d01c //sprite MC
    sta $d015

    sei
    lda #$35    //fuck kernal and basic!
    sta $01

    lda #$7f
    sta $dc0d
    sta $dd0d

    lda $dc0d
    lda $dd0d

    lda #<irq
    sta $fffe
    lda #>irq
    sta $ffff
    lda #$01
    sta $d01a
    lda #IRQ_LINE_0
    sta $d012
    lda $d011
    and #$7f
    sta $d011
    cli
}

.macro enterIRQ(){
    pha
    txa
    pha
    tya
    pha      
}

.macro openBorder(){
    lda #$33
    sta $d011
    lda #IRQ_LINE_1
!:  cmp $d012
    bne !-
    lda #$3b
    sta $d011
}

.macro stabilizeRaster(){
    lda #<WedgeIRQ
    sta $fffe
    lda #>WedgeIRQ
    sta $ffff

    inc $d012
    lda #$bb
    sta $d011

    // Acknowlege current Raster IRQ
    lda #$01
    sta $d019
    tsx
    cli
    .fill 11,NOP    //enough for NTSC and PAL
    WedgeIRQ:
    // At this point the next Raster Compare IRQ has triggered and the jitter is max 1 cycle.
    // CYCLECOUNT: [7 -> 8] (7 cycles for the interrupt handler + [0 -> 1] cycle Jitter for the NOP)

    // Restore previous Stack Pointer (ignore the last Stack Manipulation by the IRQ)
    txs

    // PAL-63  // NTSC-64    // NTSC-65
    //---------//------------//-----------
@NTSCCompensateByte1:
    ldx #$08   // ldx #$08   // ldx #$09
    dex        // dex        // dex
    bne *-1    // bne *-1    // bne *-1
    bit $00    // nop
               // nop

    // Check if $d012 is incremented and rectify with an aditional cycle if neccessary
    lda $d012
    cmp $d012  // <- critical instruction (ZERO-Flag will indicate if Jitter = 0 or 1)

    // CYCLECOUNT: [61 -> 62] <- Will not work if this timing is wrong

    // cmp $d012 is originally a 5 cycle instruction but due to piplining tech. the
    // 5th cycle responsible for calculating the result is executed simultaniously
    // with the next OP fetch cycle (first cycle of beq *+2).

    // Add one cycle if $d012 wasn't incremented (Jitter / ZERO-Flag = 0)
    beq *+2
}

.macro leaveIRQ(){
    //reset original irq line
    lda #<irq
    sta $fffe
    lda #>irq
    sta $ffff

    lda #IRQ_LINE_0
    sta $d012
    lda #$3b
    sta $d011

    lda #$ff
    sta $d019
    pla
    tay   
    pla
    tax   
    pla   

    rti   
}