*=$0801    
    BasicUpstart($080d)

.var sid= LoadSid("Haunted_House.sid")
.const bitmapAt = $4000
.const charAt = $6000
.const SPRITE_COLOR = WHITE//ORANGE
.const IRQ_LINE_0 = $f8
.const IRQ_LINE_1 = $ff
.const SPRITE_START = $7000
.const SPRITE_POINTER_START = (SPRITE_START & $3fff)/64
.const BUFFER = SPRITE_START+ 8*64
.const KAR_TARGET= $8000

.const PAL_NOP = $e2
.const NTSC_NOP= NOP

.import source "macros.asm"

*=$080d
    lda #$0b
    sta $d011
    ldx #BLACK//WHITE//BLACK
    stx $d020
    stx $d021

    ldx #PAL_NOP
    ldy #8
l1: 
    lda $d012
l2: 
    cmp $d012
    beq l2
    bmi l1
    cmp #$20
    bcs pal
    lda #246
    sta spriteZeroPos + 1
    lda #$24 //BIT zp
    sta NTSCCompensateByte3
    sta NTSCCompensateByte4
    ldx #NTSC_NOP //NOOP 2 cycles, but 2 bytes do skips next NOP, so saves 2 cycles, for PAL
    ldy #9
pal:
    stx NTSCCompensateByte0
    stx NTSCCompensateByte2
    sty NTSCCompensateByte1 + 1

    //new
    copyChars()

    //copysid()
    copyCharCol()
    //copyD800()
    clearSpriteArea()
    copybitmap()
    copysid()

    lda #0
    jsr sid.init

    lda #$00
    sta $7fff

    setupSprites()

    setupIRQ()
    setBitmap()
    jmp *

irq:
    enterIRQ()
    openBorder()
    
    stabilizeRaster()

   
//turn on sprites    
    lda #$ff
    sta $d015
    ldy #$c8//#$d8

    lda #$04
!:  cmp $d012
    bne !-

//open side border for x lines
    nop
NTSCCompensateByte2:         
    nop
NTSCCompensateByte4:    
    nop
    nop
    //bit $01
    ldx #5             
!:  dex                
    bpl !-
    ldx #16
ol:    
    dec $d016
    sty $d016  
    .fill 10,NOP
NTSCCompensateByte3:
    nop
    nop
    //bit $01
NTSCCompensateByte0:
    .byte NOP
    .byte NOP    
    clc
    bit $01
    dex
    bpl ol    

    lda #$ff
    sta $d01b

    lda #$00
    sta $d015
//delay every 6th frame if NTSC
    lda NTSCCompensateByte3
    cmp #$24
    bne doPlay
    dec sidCount
    bne doPlay
    lda #6
    sta sidCount
    jmp noPlay
doPlay:
.break
    jsr sid.play
noPlay:    
//make sure sprites are off before new screento prevent them being drawn twice
    scrollSpriteData()
end:    
    leaveIRQ()
bufferCount:
    .byte 0 
curchar: 
    .byte 0
    .byte 0    
sidCount:
    .byte 6    
scrollText:
    .encoding "screencode_upper"
    //              1                                       5
    //     1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
    //.text "LIKE AN OLD DRAGON ON LONELY MOUNTAIN I KEPT ALL MY OLD SHIT TO MYSELF, UNUSED,UNSPREAD. NO MORE! BETTER OUT THAN IN, I SAY! CSDB WILL RUE THE DAY, BUT HERE'S THIS VERY NICE PIC I RIPPED LONG AGO THAT I CAME ACROSS WHILE LOOKING FOR ONE OF MY OLD GAMES (WHICH WILL ALSO SEE THE LIGHT OF DAY SOON). I COMBINED IT WITH A CHAR SET I RIPPED FROM GOD KNOWS WHERE AND MY FIRST EVER SID TUNE MADE WITH GOATTRACKER IN 2010. IS IT BAD? SURE, BUT IT'S MY BAD! GREETINGS/BLAME TO SANDER WHO AWOKE ME FROM MY SLUMBER ;)     "    
    .text "OH NO! SIR CLIVE SINCLAIR HAS COME BACK FROM THE DEAD! AND HE'S HAUNTING YOUR COMMODORE! SWAMPINGLY SOFTENING THE KEYBOARD, HELLISHLY BRIGHTENING THE COLOURS, SPRITES SPIRITED AWAY, THE SID REDUCED TO A SCREECHING BLEEPER, COLOUR CLASHES CREEPING ALL OVER THE SCREEN! THE WORST HALLOWEEN EVER! ..... GHOULY GREETINGS TO MY FELLOW F4CG BLOATING BODY BUDDIES AND THE REST OF THE SCARY SCENE, TERRIBLE THANKS TO WOE-IS-ME-WACEK FOR LETTING ME USE HIS SID...                  BOOH!                                     "    
    // .text "... LIKE AN OLD DRAGON ON LONELY MOUNTAIN I KEPT ALL MY OLD SHIT TO MYSELF .. UNUSED .. UNSPREAD .. "
    // .text "WELL, NO MORE. BETTER OUT THAN IN, I SAY! CSDB WILL RUE THE DAY, BUT HERE'S THIS VERY NICE PIC I RIP"
    // .text "PED LONG AGO THAT I CAME ACROSS WHILE LOOKING FOR ONE OF MY OLD GAMES (WHICH WILL ALSO SEE THE LIGHT"
    // .text " OF DAY SOON). I COMBINED IT WITH A CHAR SET I RIPPED FROM GOD KNOWS WHERE AND MY FIRST EVER SID TUN"
    // .text "E MADE WITH GOATTRACKER IN 2010. IS IT BAD? SURE, BUT IT'S MY BAD! GREETINGS/BLAME TO SANDER WHO 'AW"
    // .text "OKE ME FROM MY SLUMBER' :)"    
.align $100
karload:
    //.import binary "minified_chars.bin"        
    .import binary "chars.bin"        
musicload:
    .fill sid.size, sid.getData(i) 
picload:    
    //.import binary "pic.bin"
    .import binary "5t.bin"
    
